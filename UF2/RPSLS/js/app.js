let userPoints = 0;
let computerPoints = 0;

function cambio1(event) {
    var imagen = event.target.src;
    var userChoiceDisplay = document.querySelector('#tabla1 tr:nth-child(2) td:nth-child(1)');
    userChoiceDisplay.innerHTML = '<img src="' + imagen + '" alt="foto" class="mano">';
}

function play() {
    var computerChoiceDisplay = document.querySelector('#tabla1 tr:nth-child(2) td:nth-child(2)');
    computerChoiceDisplay.innerHTML = '';

    var computerChoices = ['rock', 'paper', 'scissors', 'lizard', 'spock'];
    var timeoutDuration = 50;
    var maxTimeouts = 15;

    function rotar() {
        var randomIndex = Math.floor(Math.random() * computerChoices.length);
        var computerChoice = computerChoices[randomIndex];
        computerChoiceDisplay.innerHTML = '<img src="assets/images/' + computerChoice + '.png" alt="foto" class="mano">';
        maxTimeouts--;
        if (maxTimeouts > 0) {
            setTimeout(rotar, timeoutDuration);
        } else {
            determinarGanador(computerChoice);
        }
    }

    function determinarGanador(computerChoice) {
        var userChoiceDisplay = document.querySelector('#tabla1 tr:nth-child(3) td:nth-child(1)');
        var computerChoiceDisplay = document.querySelector('#tabla1 tr:nth-child(3) td:nth-child(2)');

        userChoiceDisplay.innerHTML = 'Puntos: ' + userPoints;
        computerChoiceDisplay.innerHTML = 'Puntos: ' + computerPoints;

        var userChoice = document.querySelector('#tabla1 tr:nth-child(2) td:nth-child(1) img').src;

        userChoice = userChoice.substring(userChoice.lastIndexOf('/') + 1, userChoice.lastIndexOf('.png'));

        var resultDisplay = document.getElementById("result");

        if (userChoice === computerChoice) {
            resultDisplay.innerText = "Resultado : Emptate!";        
        } else if (
            (userChoice == "rock" && (computerChoice == "scissors" || computerChoice == "lizard")) ||
            (userChoice == "paper" && (computerChoice == "rock" || computerChoice == "spock")) ||
            (userChoice == "scissors" && (computerChoice == "paper" || computerChoice == "lizard")) ||
            (userChoice == "lizard" && (computerChoice == "spock" || computerChoice == "paper")) ||
            (userChoice == "spock" && (computerChoice == "rock" || computerChoice == "scissors"))
        ) {
            resultDisplay.innerText = "Resultado: El jugador Gano!";            
            userPoints++;
        } else {
            resultDisplay.innerText = "Resultado: El ordenador Gano!";
            computerPoints++;
        }

        document.getElementById("point1").innerText = "Puntos: " + userPoints;
        document.getElementById("point2").innerText = "Puntos: " + computerPoints;
    }

    setTimeout(rotar, timeoutDuration);
}





