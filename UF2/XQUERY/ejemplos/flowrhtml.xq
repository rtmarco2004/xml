<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Alumnes</title>
</head>
<body>
    <h1>Alumnes</h1>
    <ul>
        {
        for $x in doc("institut.xml")/institut/alumnes/alumne
        order by $x
        return
        <li>
            {$x/nom}
            {$x/cognoms}
        </li>
        }
    </ul>
</body>
</html>