<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Alumnes</title>
</head>
<body>
    <h1>Alumnes</h1>
    <table border="2px solid">
    <thead>
      <tr>
          <th>Nom</th>
          <th>Cognom</th>
          <th>Edat</th>
      </tr>
    </thead>
    <tbody>
        {
        for $x in doc("institut2.xml")/institut/alumnes/alumne
        order by $x
        return
        <tr>
                 <td>
        {
            let $image := $x/image
            return
            <img  src="{ data($image)}" width="20%"></img>
        }
       </td>
          <td>{$x/nom}</td>
          <td>{$x/cognoms}</td>
          <td>{$x/edat}</td>
        </tr>
        }
        </tbody>
    </table>
</body>
</html>