let $prices := doc("ex1.xml")/autos/vehicles/vehicle[price > 20000 and price < 30000]/price
return avg($prices)