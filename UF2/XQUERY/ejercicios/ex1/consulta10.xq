for $vehicle in doc("ex1.xml")/autos/vehicles/vehicle
where $vehicle/price > 20000
order by $vehicle/price ascending
return concat(data($vehicle/model), ' - ', data($vehicle/price))