for $i in doc("ex1.xml")/autos/vehicles/vehicle
order by $i/price ascending
return (data($i/model),data($i/price))