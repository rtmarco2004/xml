<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Tenda de llibres</title>
    <style>
        <![CDATA[
            th {
                background-color: skyblue;
            }

            tr:nth-child(odd) {
                background-color: lightgray;
            }
        ]]>
    </style>
</head>
<body>
    <h1>Book List</h1>
    <table border="2px solid orange">
        <tr>
            <th>Title</th>
            <th>Editorial</th>
            <th>Price</th>
        </tr>
        {
            let $books := doc("ex3.xml")/bookstore/book
            for $x in $books
            return
            <tr>
                <td>{data($x/title)}</td>
                <td>{data($x/editorial)}</td>
                <td>{data($x/price)}</td>
            </tr>
        }
    </table>
</body>
</html>

