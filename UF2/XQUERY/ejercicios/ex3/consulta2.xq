<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Tenda de llibres</title>
    <style>
        <![CDATA[
            th {
                background-color: skyblue;
            }

            tr:nth-child(odd) {
                background-color: lightgray;
            }
            
            h1 {
              background-color: skyblue;
            }
            
            .hola {
              text-align: right;
            }
        ]]>
    </style>
</head>
<body>
    <h1>Stevens Books</h1>
    <table border="2px solid orange">
        <tr>
            <th>Title</th>
            <th>Year</th>
            <th>Authors</th>
            <th>Editorial</th>
            <th>Price</th>
        </tr>
        {
            let $books := doc("ex3.xml")/bookstore/book
            for $x in $books
              where $x/author/lastname = "Abiteboul"
            return
            <tr>
                <td>{data($x/title)}</td>
                <td>{data($x/@year)}</td>
                <td>{data($x/author)}</td>
                <td>{data($x/editorial)}</td>
                <td>{data($x/price)}</td>
            </tr>
        }
        <tr class="hola">
          <td colspan="5">Total price: {sum(for $price in doc("ex3.xml")/bookstore/book where $price/author/lastname ="Abiteboul" return $price/price)}</td>
        </tr>
    </table>
</body>
</html>

