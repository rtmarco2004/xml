let $doc := doc("ex4.xml")

for $i in $doc /library/book
where $i/@year > 1992 and $i/editorial = "Addison-Wesley"

return (data($i/title), data($i/editorial), data($i/@year))