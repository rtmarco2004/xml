let $doc := doc("ex4.xml")

for $i in $doc /library/book
where $i/@year < 2000

return (data($i/title), data($i/@year))