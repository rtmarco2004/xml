let $doc := doc("ex4.xml")

for $i in $doc /library/book
where $i/price = 65.95

return (data($i/title), data($i/price))