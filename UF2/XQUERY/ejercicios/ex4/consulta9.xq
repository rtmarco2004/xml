let $doc := doc("ex4.xml")

for $i in $doc /library/book

return <result>{$i/title,$i/author}</result>