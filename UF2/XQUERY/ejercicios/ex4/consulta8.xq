let $doc := doc("ex4.xml")

let $unic := distinct-values($doc /library/book/author/lastname)
for $i in $unic 
order by $i
return $i