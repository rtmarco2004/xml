let $doc := doc("ex4.xml")

for $i in $doc /library/book
order by $i/@year

return (data($i/title), data($i/@year))