let $doc := doc("ex4.xml")

for $i in $doc /library/book
where count($i/author) > 1

return (data($i/title), data($i/@year))