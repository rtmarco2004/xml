let $doc := doc("ex4.xml")

for $i in $doc /library/book

return <llibre>{$i/title,count($i/author)}</llibre>