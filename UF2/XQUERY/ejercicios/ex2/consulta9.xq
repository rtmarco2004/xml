declare function local:expenses($price as xs:decimal) as xs:decimal {
    let $iva := $price * 0.21
    let $transportTax := 2.5
    return fn:round($iva + $transportTax, 2)
};

let $doc := doc("ex2.xml")
for $game in $doc/playstore/games/game
return
    <product>
        <name>{$game/name}</name>
        <price>{$game/price}</price>
        <expenses>{local:expenses(xs:decimal($game/price))}</expenses>
    </product>






