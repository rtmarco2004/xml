let $doc := doc("ex2.xml")

let $diferent := distinct-values($doc/playstore/games/game/platform)
return count($diferent)