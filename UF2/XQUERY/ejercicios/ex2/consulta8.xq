let $doc := doc("ex2.xml")
for $i in $doc /playstore/games/game

order by $i/price ascending

return if($i/price>50 and $i/price<50)
then <offer>{$i/name}</offer>
else <standard>{$i/name, $i/price}</standard>