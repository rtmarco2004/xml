let $doc := doc("ex2.xml")

return <game>
{
  for $x in $doc/playstore/games/game
  where $x/age >= 18
  return <adult-game>{$x/name,$x/price}</adult-game>
}
</game>