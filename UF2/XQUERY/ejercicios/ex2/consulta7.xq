let $doc := doc("ex2.xml")
for $i in $doc /playstore/games/game
where contains(lower-case($i/name), "super")
return ($i/name, $i/company)