let $doc := doc("pokedex.xml")

return 
<Pokemons>
{
for $i in $doc /pokedex/pokemon
where $i/baseStats/ATK > 100
order by $i/baseStats/ATK
return <pokemon>{$i/species,$i/baseStats/ATK}</pokemon>
}
</Pokemons>