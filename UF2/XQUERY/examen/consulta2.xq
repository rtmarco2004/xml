let $doc := doc("pokedex.xml")

return 
<Pokemons>
{
for $i in $doc /pokedex/pokemon
order by $i/species
return <pokemon>{<name>$i/species</name>,$i/abilities}</pokemon>
}
</Pokemons>