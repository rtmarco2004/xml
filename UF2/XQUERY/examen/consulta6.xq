<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Alumnes</title>
   <link rel="stylesheet" href="style.css" />
</head>

<body>
<h1>Pokemons</h1>
<table>
        <thead>
        <tr class="part1">
            <th>Image</th>
            <th>Pokemon</th>
            <th>HP</th>
            <th>ATK</th>
            <th>DEF</th>
            <th>SPD</th>
            <th>SATK</th>
            <th>SDEF</th>
        </tr>
        </thead>
        <tbody>
        {
            for $x in doc("pokedex.xml")/pokedex/pokemon
            order by $x
            return
                 <tr class="part2">
       <td>
        {
            let $image := $x/image
            return
            <img  src="{ data($image)}" width="80%"></img>
        }
       </td>
          <td>{ $x/species}</td>
          <td>{ $x/baseStats/HP}</td>
          <td>{ $x/baseStats/ATK}</td>
          <td>{ $x/baseStats/DEF}</td>
          <td>{ $x/baseStats/SPD}</td>
          <td>{ $x/baseStats/SATK}</td>
          <td>{ $x/baseStats/SDEF}</td>
          </tr>
    }
    </tbody>
     </table>
</body>
</html>