let $doc := doc("pokedex.xml")

return 
<Pokemons>
{
for $i in $doc /pokedex/pokemon
let $total := $i/baseStats/HP + $i/baseStats/ATK + $i/baseStats/DEF + $i/baseStats/SPD + $i/baseStats/SATK + $i/baseStats/SDEF
order by $total descending
return <pokemon>{$i/species,$total}</pokemon>
}
</Pokemons>