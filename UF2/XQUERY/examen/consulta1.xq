let $doc := doc("pokedex.xml")

for $i in $doc /pokedex/pokemon
return data($i/species)