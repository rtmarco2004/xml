<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="book" >
    Category: <xsl:value-of select="@category"/>
    Title: <xsl:value-of select="title"/>
    <xsl:text>
        &#169; copyright 2023.
       </xsl:text>
    </xsl:template>
</xsl:stylesheet>
