<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        
        <h1>Bookstore - Conditional</h1>
        <xsl:for-each select="bookstore/book">
            <ul>
                <li>Title: <xsl:value-of select="title" /></li>
                <li>Author: <xsl:value-of select="author" /></li>
                <li>Year: <xsl:value-of select="year" /></li>
                
                <xsl:if test="price >=35">
                    <li style="color: red">Price: <xsl:value-of select="price"/></li>
                </xsl:if>
                <xsl:if test="price < 35">
                    <li style="color: green">Price: <xsl:value-of select="price"/></li>
                </xsl:if>
            </ul>
            <hr />
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>