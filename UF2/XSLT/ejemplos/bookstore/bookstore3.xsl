<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        <html>
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
            </head>
            <style>
                td{border: 1px solid black}
            </style>
            <body>
                <h2>Books</h2>
                
                <table style="border: 1px solid black">
                    <thead>
                        <tr bgcolor="red">
                            <td>Title</td>
                            <td>Author</td>
                            <td>year</td>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="bookstore/book">
                        <xsl:sort select="title">
                            
                        </xsl:sort>
                            <tr>
                                <td>
                                    <xsl:value-of select="title"/><br />
                                </td>
                                <td>
                                    <xsl:value-of select="author"/>
                                </td>
                                <td>
                                    <xsl:value-of select="year"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>