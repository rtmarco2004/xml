<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head><title>XSL Transform</title><meta charset="UTF-8" /></head>
            <body>
                <h2>Bookstore choose-when</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Title</th>
                        <th>Artist</th>
                        <th>Year</th>
                        <th>Price</th>
                    </tr>
                    <xsl:for-each select="bookstore/book">
                        <tr>
                            <xsl:choose> 
                                <xsl:when test="price>=35">
                                    <td bgcolor="cyan">
                                        <xsl:value-of select="title" />
                                    </td>
                                    <td bgcolor="cyan">
                                        <xsl:value-of select="author" />
                                    </td>
                                    <td bgcolor="cyan">
                                        <xsl:value-of select="year" />
                                    </td>
                                    <td bgcolor="red">
                                        <xsl:value-of select="price" />
                                    </td>
                                </xsl:when>
                                <xsl:otherwise> <td bgcolor="yellow">
                                        <xsl:value-of select="title" />
                                    </td>
                                    <td bgcolor="yellow">
                                        <xsl:value-of select="author" />
                                    </td>
                                    <td bgcolor="yellow">
                                        <xsl:value-of select="year" />
                                    </td>
                                    <td bgcolor="green">
                                        <xsl:value-of select="price" />
                                    </td>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>