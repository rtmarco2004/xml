<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" 
                indent="yes"
                media-type="text/html"
                encoding="utf-8"
                doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
    
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>XSL Transform</title>
            </head>
            <body>
                <h2>Header</h2>
                <!-- content -->
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

