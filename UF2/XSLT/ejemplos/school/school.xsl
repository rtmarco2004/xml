<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Atributte img</title>
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <table border="1">
                    <thead>
                        <tr bgcolor="red">
                            <th>Image</th>
                            <th >Name</th>
                            <th >Email</th>
                        </tr>
                    </thead>
                    <tbody>

                        <xsl:for-each select="school/students/student">

                            <tr>
                                <td>
                                    <xsl:variable name="imageSrc" select="image" />
                                    <xsl:variable name="imageTitle" select="image"/>
                                    <img  title="{$imageTitle}" alt="{ $imageTitle}" src="{$imageSrc}" style="width: 100px; height: 100px;"/>
                                </td>
                                <td><b><xsl:value-of select="name" /></b></td>

                                <td><xsl:value-of select="email" /></td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>