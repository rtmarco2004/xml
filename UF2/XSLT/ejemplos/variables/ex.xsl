<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Variables</title>
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <table>
                    <thead>

                        <xsl:variable name="color" select="'red'" /> 
                            <tr>
                                <th bgcolor="{$color}">ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Salary</th>
                                <th>Bonus</th>
                                <th>Total</th>
                            </tr>

                    </thead>
                    <tbody>
                        <xsl:for-each select="company/employees/employee">          
                            <tr>
                                <td><b><xsl:value-of select="id" /></b></td>                    
                                <td><b><xsl:value-of select="name" /></b></td>  
                                <td><xsl:value-of select="email" /></td>
                                <td><xsl:value-of select="salary" /></td>
                                <td><xsl:value-of select="bonus" /></td>
                                <td>
                                    <xsl:variable name="salary">
                                        <xsl:value-of select="salary"/> 
                                    </xsl:variable>
                                    <xsl:variable name="bonus">
                                        <xsl:value-of select="bonus"/> 
                                    </xsl:variable> 
                                    <xsl:value-of select="$salary + $bonus"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>