<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            
            
            
            <style>
                
                h1{
                text-align: center;
                }
                table{
                border-collapse: collapse;
                float: center;
                margin: auto;
                box-shadow: 0 0 25px rgba(0, 0, 0, 0.5);
                margin-top: 2.75%;
                }
                th,td{
                text-align: center;
                padding: 15px;
                font-family: 'Open Sans', sans-serif;
                }
                
                
                th{
                background-color: cyan;
                }
                
                h3{
                text-align: center;
                }
                
                body{
                text-align: center;
                }
                
            </style>
            <body>
                <img style="width:100px; height:100px" src="logo_UEFA.png"></img>
                <h1>UEFA Champions League</h1>
                <table>
                    
                    <tr>
                        <th colspan="7">Quarter finals</th>
                        <th>Winner</th>
                    </tr>
                    
                    <xsl:for-each select="UEFA/season/quarter-finals/match">
                        <tr>
                            <td>
                                <xsl:variable name="img1" select="first-leg/local/logo" />
                                <img  title="{$img1}" alt="{$img1}" src="{$img1}" style="width: 75px; height: 75px;"/>
                            </td>
                            <td><xsl:value-of select="first-leg/local/name"/></td>
                            <td><xsl:value-of select="first-leg/local/goals"/></td>
                            <td>-</td>
                            <td><xsl:value-of select="first-leg/visitant/goals"/></td>
                            <td><xsl:value-of select="first-leg/visitant/name"/></td>
                            <td>
                                <xsl:variable name="img1" select="first-leg/visitant/logo" />
                                <img  title="{$img1}" alt="{$img1}" src="{$img1}" style="width: 75px; height: 75px;"/>
                            </td>
                            <xsl:choose>
                                <xsl:when test="winner=A">
                                    <td rowspan="2">
                                        <xsl:variable name="img1" select="first-leg/local/logo" />
                                        <img  title="{$img1}" alt="{$img1}" src="{$img1}" style="width: 75px; height: 75px;"/>
                                        <xsl:value-of select="first-leg/local/name"/>
                                    </td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td rowspan="2">
                                        <xsl:variable name="img1" select="second-leg/visitant/logo" />
                                        <img  title="{$img1}" alt="{$img1}" src="{$img1}" style="width: 75px; height: 75px;"/>
                                        <xsl:value-of select="first-leg/visitant/name"/>
                                    </td>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tr>
                        <tr>
                            <td>
                                <xsl:variable name="img1" select="second-leg/local/logo" />
                                <img  title="{$img1}" alt="{$img1}" src="{$img1}" style="width: 75px; height: 75px;"/>
                            </td>
                            <td><xsl:value-of select="second-leg/local/name"/></td>
                            <td><xsl:value-of select="second-leg/local/goals"/></td>
                            <td>-</td>
                            <td><xsl:value-of select="second-leg/visitant/goals"/></td>
                            <td><xsl:value-of select="second-leg/visitant/name"/></td>
                            <td>
                                <xsl:variable name="img1" select="second-leg/visitant/logo" />
                                <img  title="{$img1}" alt="{$img1}" src="{$img1}" style="width: 75px; height: 75px;"/>
                            </td> 
                            
                        </tr>
                    </xsl:for-each>
                </table>
                <h3>Total goals: <xsl:value-of select="sum(//goals)"/></h3>
                <h3>Number of matches: <xsl:value-of select="count(//match/first-leg)+count(//match/second-leg)"/></h3>
                <h3>Average goals per match: <xsl:value-of select="(sum(//goals)div(count(//match/first-leg)+count(//match/second-leg)))"/></h3>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>