<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        <html>
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
            </head>
            <style>
                table{
                    border-collapse: collapse;
                }
                th,td{
                    border: 1px solid black;
                    text-align: center;
                    padding: 8px}

                tr:nth-child(even) {
                    background-color: #c3c3c3;
                }

                th{
                    background-color: cyan;
                }
            </style>
            <body>
                <h2>Film List</h2>
                
                <table style="border: 1px solid black">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Language</th>
                            <th>Year</th>
                            <th>Country</th>
                            <th>Genre</th>
                            <th>Director</th>
                            <th>Birth_date</th>
                            <th>Summary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="films/film">
                        <xsl:sort select="title">
                            
                        </xsl:sort>
                            <tr>
                                <td>
                                    <xsl:value-of select="title"/><br />
                                </td>
                                <td>
                                    <xsl:value-of select="title/@lang"/><br />
                                </td>
                                <td>
                                    <xsl:value-of select="year"/>
                                </td>
                                <td>
                                    <xsl:value-of select="country"/>
                                </td>
                                <td>
                                    <xsl:value-of select="genre"/>
                                </td>
                                <td>
                                    <xsl:value-of select="concat(director/first_name, ' ', director/last_name)"/>
                                </td>
                                <td>
                                    <xsl:value-of select="director/birth_date"/>
                                </td>
                                <td>
                                    <xsl:value-of select="summary"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>