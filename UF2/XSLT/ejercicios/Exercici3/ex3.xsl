<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    
    <xsl:template match="/">
        <h1>Products with a Price > 25 and Price 100</h1>
        <style>
            table{
                    border-collapse: collapse;
                    float: center;
                    margin: auto;
                    box-shadow: 0 0 25px rgba(0, 0, 0, 0.5);
                    margin-top: 2.75%;
                    border-radius: 10px;
                    overflow: hidden;
            }
            
            #tabla1 th, td{
                padding: 20px;
                text-align: center;
            }


            #tabla2 th, td{
                padding: 5px;
                text-align: center;
                width: 400px;
            }

            h1{
                text-align: center;
            }

            #tabla1 tr:nth-child(odd) {
                    background-color: #c3c3c3;
                }

            #cosa1 {
                background-color: cyan;
            }

        </style>
        <body>

        <table id="tabla1">
                    <thead>
                        <tr>
                            <th colspan="2" bgcolor="#4eb1fa">Customer Details</th>
                        </tr>
                        <tr>
                            <th bgcolor="#e3ecf3">Name</th>
                            <td bgcolor="#0088ec"><xsl:value-of select="order/destination/name" /></td>
                        </tr>
                        <tr>
                            <th bgcolor="#e3ecf3">Address</th>
                            <td><xsl:value-of select="order/destination/address" /></td>
                        </tr>
                        <tr>
                            <th bgcolor="#e3ecf3">City</th>
                            <td><xsl:value-of select="order/destination/city" /></td>
                        </tr>
                        <tr>
                            <th bgcolor="#e3ecf3">P.C.</th>
                            <td><xsl:value-of select="order/destination/postalcode" /></td>
                        </tr>
                    </thead>
        </table>
        <table border="0" style="border-collapse: collapse;" id="tabla2">
            <thead>
                <tr>
                    <th colspan="4" id="cosa1">Order</th>
                </tr>
            </thead>
            <tr>
                <th>Product</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
            </tr>
            <xsl:for-each select="order/products/product[price > 25 and price <= 100]">
                <tr>
                    <xsl:choose>
                        <xsl:when test="price > 25 and price < 50">
                            <xsl:attribute name="style">background-color: yellow;</xsl:attribute>
                        </xsl:when>
                        <xsl:when test="price >= 50 and price < 75">
                            <xsl:attribute name="style">background-color: green;</xsl:attribute>
                        </xsl:when>
                        <xsl:when test="price >= 75 and price <= 100">
                            <xsl:attribute name="style">background-color: red;</xsl:attribute>
                        </xsl:when>
                    </xsl:choose>
                    <td>
                        <xsl:value-of select="concat(name, ' (code = ', @code, ')')" />
                    </td>
                    <td>
                        <xsl:value-of select="quantity" />
                    </td>
                    <td>
                        <xsl:value-of select="price" />
                    </td>
                    <td>
                        <xsl:value-of select="quantity * price" />
                    </td>
                </tr>
            </xsl:for-each>
        </table>
        </body>
    </xsl:template>
</xsl:stylesheet>


