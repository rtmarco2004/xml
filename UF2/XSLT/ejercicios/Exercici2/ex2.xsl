<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Attribute img</title>
                <link rel="stylesheet" href="style.css" />
            </head>

            <style>
                table{
                    border-collapse: collapse;
                    float: center;
                    margin: auto;
                    box-shadow: 0 0 25px rgba(0, 0, 0, 0.5);
                    margin-top: 2.75%;
                }
                th,td{
                    text-align: center;
                    padding: 15px;
                    font-family: 'Open Sans', sans-serif;
                    }

                tr:nth-child(even) {
                    background-color: #c3c3c3;
                }

                th{
                    background-color: cyan;
                }

                tr:hover{
                    background-color: rgba(0, 0, 0, 0.3);
                }

                td:nth-child(2) {
                    font-weight: bold;
                }
            </style>

            <body>
                <table>
                    <thead>
                        <tr >
                            <th>Login</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>License</th>
                        </tr>
                    </thead>
                    
                    <tbody> 
                        <xsl:for-each select="coding/programs/program">     
                            <tr>
                                <td>
                                    <xsl:variable name="img1" select="logo" />
                                    <img  title="{$img1}" alt="{$img1}" src="{$img1}" style="width: 75px; height: 75px;"/>
                                </td>
                                <td><xsl:value-of select="name" /></td>
                                <td><xsl:value-of select="type" /></td>
                                <td><xsl:value-of select="license" /></td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>