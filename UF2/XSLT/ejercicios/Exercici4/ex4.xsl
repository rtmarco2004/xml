<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    
    <xsl:template match="/">
        <h1>Bookstore</h1>
        <style>
            table {
                border-collapse: collapse;
                float: center;
                margin: auto;
                box-shadow: 0 0 25px rgba(0, 0, 0, 0.5);
                margin-top: 4%;
            }
            
            th, td {
                width: 300px;
                padding: 20px;
            }
            
            th {
                text-align: left;
                background-color: #bbdbf3;
            }

            #fila2 {
                background-color: #59adec;
            }

            .cosa1:nth-child(even) {
                background-color: #e4e7e8;
            }

            #cosa {
                background-color: #e4e7e8;
            }
        </style>
        <xsl:for-each select="bookstore/book">
            <table border="0" style="border-collapse: collapse;">
                <tr class="cosa1">
                    <th>
                        <xsl:value-of select="concat('Title (lang: ', title/@lang,')' )"/>
                    </th>
                    <td>
                        <xsl:value-of select="title"/>
                    </td>
                </tr>
                <tr class="cosa1">
                    <th>Category</th>
                    <td>
                        <xsl:value-of select="@category"/>
                    </td>
                </tr>
                <tr class="cosa1">
                    <th>Year</th>
                    <td>
                        <xsl:value-of select="year"/>
                    </td>
                </tr>
                <tr class="cosa1">
                    <th>Price</th>
                    <td>
                        <xsl:value-of select="concat(price, ' €')"/>
                    </td>
                </tr>
                <tr class="cosa1">
                    <th>Format</th>
                    <td>
                        <xsl:value-of select="format/@type"/>
                    </td>
                </tr>
                <tr class="cosa1">
                    <th>ISBN</th>
                    <td>
                        <xsl:value-of select="isbn"/>
                    </td>
                </tr>
                <tr>
                    <th id="fila2" colspan="2">Authors:</th>
                </tr>
                <xsl:for-each select="author">
                    <xsl:sort select="."/>
                    <tr>
                        <td id="cosa" colspan="2">
                            <xsl:value-of select="."/>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
